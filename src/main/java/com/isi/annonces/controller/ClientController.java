package com.isi.annonces.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isi.annonces.entities.Client;
import com.isi.annonces.services.ClientService;

import lombok.AllArgsConstructor;

@RestController
@CrossOrigin("*")
@RequestMapping("/client")
@AllArgsConstructor
public class ClientController {

	@Autowired
	private ClientService clientservice;
	
	@PostMapping("/ajout")
	public Client ajouter (@RequestBody Client client) {
		return clientservice.save(client);
	}
	
	@GetMapping("/list")
	public List<Client> lire ( Client client) {
		return clientservice.findAll();
	}
	
	@PutMapping("/modifier/{id}")
	public Client modifier (@PathVariable Long id, @RequestBody Client client) {
		return clientservice.modify(id, client);
	}
	
	@GetMapping("/nom/{nom}")
	public Optional<Client> obtenir (@PathVariable String nom, Client client) {
		return clientservice.findByNom(nom);
	}


	@GetMapping("/prenom/{prenom}")
	public Client obtenir2 (@PathVariable String prenom, Client client) {
		return clientservice.findByPrenom(prenom);
	}
	
	@DeleteMapping("/delete/{id}")
	public void supprimer (@PathVariable Long id) {
		clientservice.delete(id);;
	}
	
	@GetMapping("/{id}")
	public Optional<Client> obtenirId (@PathVariable Long id, Client client) {
		return clientservice.findById(id);
	}
}
