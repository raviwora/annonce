package com.isi.annonces.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isi.annonces.entities.Admin;
import com.isi.annonces.services.AdminService;

import lombok.AllArgsConstructor;

@RestController
@CrossOrigin("*")
@RequestMapping("/admin")
@AllArgsConstructor
public class AdminController {

	@Autowired
	private AdminService adminservice;
	
	@PostMapping("/ajout")
	public Admin ajouter (@RequestBody Admin admin) {
		return adminservice.save(admin);
	}
	
	@GetMapping("/list")
	public List<Admin> lire ( Admin admin) {
		return adminservice.findAll();
	}
	
	@PutMapping("/modifier/{id}")
	public Admin modifier (@PathVariable Long id, @RequestBody Admin admin) {
		return adminservice.modify(id, admin);
	}
	
	@GetMapping("/nom/{nom}")
	public Optional<Admin> obtenir (@PathVariable String nom, Admin admin) {
		return adminservice.findByNom(nom);
	}


	@GetMapping("/prenom/{prenom}")
	public Admin obtenir2 (@PathVariable String prenom, Admin admin) {
		return adminservice.findByPrenom(prenom);
	}
	
	@DeleteMapping("/delete/{id}")
	public void supprimer (@PathVariable Long id) {
	  adminservice.delete(id);;
	}
	
	@GetMapping("/{id}")
	public Optional<Admin> obtenirId (@PathVariable Long id, Admin admin) {
		return adminservice.findById(id);
	}
}
