package com.isi.annonces.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.isi.annonces.entities.Annonce;
import com.isi.annonces.entities.Image;
import com.isi.annonces.services.AnnonceService;

@RestController
@CrossOrigin("*") //permet à l'api front de pouvoir se connecter au back
@RequestMapping("/annonce")
public class AnnonceController {

		@Autowired
		private AnnonceService annonceservice;
		
		public AnnonceController(AnnonceService annonceservice) {
			super();
			this.annonceservice = annonceservice;
		}
		
		@PostMapping(value= {"/ajout"}, consumes= {MediaType.MULTIPART_FORM_DATA_VALUE})
		public Annonce ajouter (@RequestPart("annonce") Annonce annonce,
								@RequestPart("imagefile") MultipartFile[] file ) {
			
			try {
				Set<Image> images= uploadImage(file);
				annonce.setPhotos(images);
				return annonceservice.save(annonce);
			}catch (Exception e){
				System.out.println(e.getMessage());
				return null;
			}
		}
		
		public Set<Image> uploadImage(MultipartFile[] multipartFiles) throws IOException {
			Set<Image> images= new HashSet<>();
			
			for (MultipartFile file: multipartFiles) {
				Image image= new Image(
						file.getOriginalFilename(),
						file.getContentType(),
						file.getBytes()
				);
				 images.add(image);
			}
			 return images;
		}
		
		@GetMapping("/list")
		public List<Annonce> lire ( Annonce annonce) {
			return annonceservice.findAll();
		}
		
		@PutMapping("/modifier/{id}")
		public Annonce modifier (@PathVariable Long id, @RequestBody Annonce annonce) {
			return annonceservice.modify(id, annonce);
		}
		
		@DeleteMapping("/delete/{id}")
		public void supprimer (@PathVariable Long id) {
			annonceservice.delete(id);;
		}
		
		@GetMapping("/{id}")
		public Annonce obtenir (@PathVariable Long id, Annonce annonce) {
			return annonceservice.findById(id);
		}
}
