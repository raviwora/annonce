package com.isi.annonces.controller;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.isi.annonces.entities.Categorie;
import com.isi.annonces.entities.Image;
import com.isi.annonces.services.CategorieService;

@RestController
@CrossOrigin("*")
@RequestMapping("/categorie")
public class CategorieController {

		@Autowired
		private CategorieService categorieservice;
		
		public CategorieController(CategorieService categorieService) {
			super();
			this.categorieservice = categorieService;
		}
		
		@PostMapping(value= {"/ajout"}, consumes= {MediaType.MULTIPART_FORM_DATA_VALUE})
		public Categorie ajouter (@RequestPart("categorie") Categorie categorie,
								@RequestPart("imagefile") MultipartFile file ) {
			
			try {
				Image image= uploadImage(file);
				categorie.setPhoto(image);
				return categorieservice.save(categorie);
			}catch (Exception e){
				System.out.println(e.getMessage());
				return null;
			}
		}
		
		public Image uploadImage(MultipartFile file) throws IOException {
				Image image= new Image(
						file.getOriginalFilename(),
						file.getContentType(),
						file.getBytes()
						);
			 return image;
		}
		
		@GetMapping("/list")
		public List<Categorie> lire ( Categorie categorie) {
			return categorieservice.findAll();
		}
		
		@GetMapping("/{nom}")
		public Optional<Categorie> obtenir (@PathVariable String nom, Categorie categorie) {
			return categorieservice.findByNom(nom);
		}
		
		@PutMapping("/modifier/{id}")
		public Categorie modifier (@PathVariable Long id, @RequestBody Categorie categorie) {
			return categorieservice.modify(id, categorie);
		}
		
		@DeleteMapping ("/delete/{id}")
		public void supprimer (@PathVariable Long id) {
			categorieservice.delete(id);;
		}

		@GetMapping("/{id}")
		public Categorie obtenirId (@PathVariable Long id, Categorie categorie) {
			return categorieservice.findById(id);
		}
}
