package com.isi.annonces.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.isi.annonces.entities.Commentaire;
import com.isi.annonces.services.CommentaireService;

@RestController
@CrossOrigin("*")
@RequestMapping("/commentaire")
public class CommentaireController {

	@Autowired
	private CommentaireService commentaireService;
	
	public CommentaireController (CommentaireService commentaireService) {
		super();
		this.commentaireService= commentaireService;
	}
	
	@PostMapping("/ajout")
	public Commentaire ajouter (@RequestBody Commentaire commentaire) {
		return commentaireService.save(commentaire);
	}
	
	@DeleteMapping("/delete/{id}")
	public void supprimer (@PathVariable Long id) {
		commentaireService.delete(id);
	}
}
