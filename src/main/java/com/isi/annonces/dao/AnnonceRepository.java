package com.isi.annonces.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.isi.annonces.entities.Annonce;

@RepositoryRestResource
public interface AnnonceRepository extends JpaRepository<Annonce, Long>{

	Optional<Annonce> findByTitre(String titre);

}
