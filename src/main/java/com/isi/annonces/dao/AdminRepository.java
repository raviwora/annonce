package com.isi.annonces.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.isi.annonces.entities.Admin;

@RepositoryRestResource
public interface AdminRepository extends JpaRepository<Admin, Long>{

	Optional<Admin> findByNom (String nom);
	
	Optional<Admin> findByPrenom (String prenom);
}
