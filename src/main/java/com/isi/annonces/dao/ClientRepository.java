 package com.isi.annonces.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.isi.annonces.entities.Client;

@RepositoryRestResource
public interface ClientRepository extends JpaRepository<Client, Long>{

	Optional<Client> findByNom (String nom);
	
	Optional<Client> findByPrenom (String prenom);

}
