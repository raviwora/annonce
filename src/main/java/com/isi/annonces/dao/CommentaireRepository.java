package com.isi.annonces.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.isi.annonces.entities.Commentaire;

@RepositoryRestResource
public interface CommentaireRepository extends JpaRepository<Commentaire, Long>{

}
