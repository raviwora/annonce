package com.isi.annonces.entities;

import org.springframework.web.bind.annotation.CrossOrigin;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@CrossOrigin("*")
public class Commentaire {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@CreatedDate
//	@Column(name = "DateDeCreation",nullable = false) 
//	@JsonProperty(access = Access.READ_ONLY)
//	private Date creationDate;
	
	private String contenu;

	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "idannonce")
	private Annonce annonce;
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "idclient")
	private Client client;

	@ManyToOne
	@JoinColumn(name = "idadmin")
	private Admin admin;
}
