package com.isi.annonces.entities;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@CrossOrigin("*")
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nom;
	
	private String prenom;
	
	private String numTel;
	
	private String email;
	
	@OneToMany(mappedBy = "client")
	private List<Annonce> annonces;
	

	@OneToMany(mappedBy = "client")
	private List<Commentaire> commentaires;
	
	//@JsonManagedReference
	public List<Annonce> getAnnonce() {
		return annonces;
	}
	
	//@JsonManagedReference
	public List<Commentaire> getCommentaire() {
		return commentaires;
	}
}
