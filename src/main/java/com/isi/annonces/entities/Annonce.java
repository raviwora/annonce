package com.isi.annonces.entities;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.CrossOrigin;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@CrossOrigin("*")
public class Annonce {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

//	@CreatedDate
//	@Column(name = "DateDeCreation",nullable = false) 
//	@JsonProperty(access = Access.READ_ONLY)
//	private Date dateCreationn;
	
    private String titre;
	
	private Integer prix;
	
	private String description;
	
	private String type;
	
	@ManyToMany(fetch= FetchType.EAGER,cascade = CascadeType.ALL)
	@JoinTable(name= "ann_im",
		joinColumns= {
				@JoinColumn(name= "ann_id")
			},
		inverseJoinColumns= {
				@JoinColumn(name="image_id")
		}
	)
	private Set<Image> photos;
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "idcategorie")
	private Categorie categorie;
	
	@ManyToOne (cascade = CascadeType.ALL)
	@JoinColumn(name = "idclient")
	private Client client;

	@OneToMany(mappedBy = "annonce")
	private List<Commentaire> commentaires;
	
	//@JsonBackReference
	public Categorie getCategorie(){
		return categorie;
	}
	
	//@JsonBackReference pour éviter les boucles infinies entres les entités
	public Client getClient() {
		return client;
	}
	
	
}
