package com.isi.annonces.entities;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@CrossOrigin("*")
public class Categorie {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nom;
	
	 @OneToOne(cascade = CascadeType.ALL)
	    @JoinColumn(name = "image_id", referencedColumnName = "id")
	private Image photo;
	
	@OneToMany(mappedBy = "categorie",cascade = CascadeType.ALL)
	private List<Annonce> annonces;
	
	//@JsonManagedReference
	public List<Annonce> getAnnonce() {
		return annonces;
	}
	
	
}
