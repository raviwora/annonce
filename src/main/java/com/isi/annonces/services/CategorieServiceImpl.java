package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isi.annonces.dao.CategorieRepository;
import com.isi.annonces.entities.Categorie;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CategorieServiceImpl implements CategorieService{

	@Autowired
	private CategorieRepository categorieRepository;
	
	@Override
	public Categorie save(Categorie categorie) {
		return categorieRepository.save(categorie);
	}
	@Override
	public Categorie modify(Long id, Categorie categorie) {
		return categorieRepository.findById(id)
				.map(c-> {
					c.setNom(categorie.getNom());
					return categorieRepository.save(c);
				}).orElseThrow(() -> new RuntimeException("Categorie non trouvé !")) ;
	}

	@Override
	public Categorie findById(Long id) {
		return categorieRepository.findById(id).orElseThrow(()-> new RuntimeException("Catégorie non trouvée !"));
	}

	@Override
	public Optional<Categorie> findByNom(String nom){
	
		Optional<Categorie> categoriefound = categorieRepository.findByNom(nom);
		if (Boolean.FALSE.equals(categoriefound.isPresent())) {
			throw new RuntimeException("La catégorie avec le nom:"+ nom +"n'existe pas");		}
		return categoriefound;
	}

	@Override
	public List<Categorie> findAll() {
		return categorieRepository.findAll();
	}

	@Override
	public void delete(Long id) {
		categorieRepository.deleteById(id);
	}
		
	}

