package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import com.isi.annonces.entities.Categorie;


public interface CategorieService {

    Categorie save (Categorie categorie);
	
    Categorie modify (Long id, Categorie categorie);
	
    Categorie findById (Long id);
	
    Optional<Categorie> findByNom (String nom);
	
	List<Categorie> findAll ();
	
	void delete (Long id);
}
