package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import com.isi.annonces.entities.Annonce;

public interface AnnonceService {


	Annonce save (Annonce annonce);
	
	Annonce modify (Long id, Annonce annonce);
	
	Annonce findById (Long id);
	
	Optional<Annonce> findByTitre (String titre);
	
	List<Annonce> findAll ();
	
	void delete (Long id);
}
