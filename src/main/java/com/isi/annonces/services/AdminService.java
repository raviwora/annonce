package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import com.isi.annonces.entities.Admin;


public interface AdminService {

	Admin save (Admin admin);
	
	Admin modify (Long id, Admin admin);
	
	Optional<Admin> findById (Long id);
	
	Optional<Admin> findByNom (String nom);
	
	Admin findByPrenom (String prenom);
	
	List<Admin> findAll ();
	
	void delete (Long id);
}
