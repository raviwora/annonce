package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isi.annonces.dao.AnnonceRepository;
import com.isi.annonces.entities.Annonce;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class AnnonceServiceImpl implements AnnonceService {

	@Autowired
	private AnnonceRepository annonceRepository;
	
	@Override
	public Annonce save(Annonce annonce) {
		return annonceRepository.save(annonce);
	}
	@Override
	public Annonce modify(Long id, Annonce annonce) {
		return annonceRepository.findById(id)
				.map(a-> {
					a.setTitre(annonce.getTitre());
					a.setDescription(annonce.getDescription());
					//a.setPhoto(annonce.getPhoto());
					a.setPrix(annonce.getPrix());
					a.setType(annonce.getType());
					return annonceRepository.save(a);
				}).orElseThrow(() -> new RuntimeException("Annonce non trouvée !")) ;
	}

	@Override
	public Annonce findById(Long id) {
		return annonceRepository.findById(id).orElseThrow(()-> new RuntimeException("Annonce non trouvée !"));
	}

	@Override
	public Optional<Annonce> findByTitre(String titre){
	
		Optional<Annonce> annoncefound = annonceRepository.findByTitre(titre);
		if (Boolean.FALSE.equals(annoncefound.isPresent())) {
			throw new RuntimeException("L'Annonce: "+ titre +" n'existe pas");		}
		return annoncefound;
	}

	@Override
	public List<Annonce> findAll() {
		return annonceRepository.findAll();
	}

	@Override
	public void delete(Long id) {
		annonceRepository.deleteById(id);
	}
	
}
