package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isi.annonces.dao.AdminRepository;
import com.isi.annonces.entities.Admin;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class AdminServiceImpl implements AdminService{

	@Autowired
	private AdminRepository adminRepository;
	
	
	@Override
	public Admin save(Admin admin) {
		return adminRepository.save(admin);
	}

	@Override
	public Admin modify(Long id, Admin admin) {
		 return adminRepository.findById(id)
				.map(a-> {
					a.setNumTel(admin.getNumTel());
					a.setEmail(admin.getEmail());
					return adminRepository.save(a);
				}).orElseThrow(() -> new RuntimeException("Admin non trouvé !")) ;
	}

	@Override
	public Optional<Admin> findById(Long id) {
		return adminRepository.findById(id);
	}

	@Override
	public Optional<Admin> findByNom(String nom){
		
		Optional<Admin> adminfound = adminRepository.findByNom(nom);
		
		if (Boolean.FALSE.equals(adminfound.isPresent())) {
			throw new RuntimeException("Admin non trouver, l'utilisateur avec le nom:"+ nom +"n'existe pas");
		}
		return adminfound;
	}
	//@Query("FROM Admin WHERE admin.nom= :nom")

	@Override
	public Admin findByPrenom(String prenom) {
		
		return adminRepository.findByPrenom(prenom)
		.orElseThrow(()-> new RuntimeException("Admin non trouver, l'utilisateur avec le prénom:"+ prenom +"n'existe pas")) ;
	}

	@Override
	public List<Admin> findAll() {
		return adminRepository.findAll();
	}

	@Override
	public void delete(Long id) {
		adminRepository.deleteById(id);
	}

}
