package com.isi.annonces.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isi.annonces.dao.CommentaireRepository;
import com.isi.annonces.entities.Commentaire;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class CommentaireServiceImpl implements CommentaireService{

	@Autowired
	private CommentaireRepository commentaireRepository;
	
	@Override
	public Commentaire save(Commentaire commentaire) {
		return commentaireRepository.save(commentaire);
	}

	@Override
	public void delete(Long id) {
		commentaireRepository.deleteById(id);
	}

}
