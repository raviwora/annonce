package com.isi.annonces.services;

import com.isi.annonces.entities.Commentaire;

public interface CommentaireService {

	Commentaire save (Commentaire commentaire);
	
	void delete (Long id);
}
