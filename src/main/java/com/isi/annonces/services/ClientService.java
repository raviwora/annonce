package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import com.isi.annonces.entities.Client;

public interface ClientService {


	Client save (Client client);
	
	Client modify (Long id, Client client);
	
	Optional<Client> findById (Long id);
	
	Optional<Client> findByNom (String nom);
	
	Client findByPrenom (String prenom);
	
	List<Client> findAll ();
	
	void delete (Long id);
}
