package com.isi.annonces.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isi.annonces.dao.ClientRepository;
import com.isi.annonces.entities.Client;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	
	@Override
	public Client save(Client client) {
		return clientRepository.save(client);
	}

	@Override
	public Client modify(Long id, Client client) {
		 return clientRepository.findById(id)
					.map(a-> {
						a.setNumTel(client.getNumTel());
						a.setEmail(client.getEmail());
						return clientRepository.save(a);
					}).orElseThrow(() -> new RuntimeException("Client non trouvé !")) ;
	}

	@Override
	public Optional<Client> findById(Long id) {
		return clientRepository.findById(id);
	}

	@Override
	public Optional<Client> findByNom(String nom){
		
		Optional<Client> clientfound = clientRepository.findByNom(nom);
		
		if (Boolean.FALSE.equals(clientfound.isPresent())) {
			throw new RuntimeException("Client non trouvé, l'utilisateur avec le nom:"+ nom +"n'existe pas");
		}
		return clientfound;
	}

	@Override
	public Client findByPrenom(String prenom) {
		
		return clientRepository.findByPrenom(prenom)
		.orElseThrow(()-> new RuntimeException("Client non trouvé, l'utilisateur avec le prénom:"+ prenom +"n'existe pas")) ;
	}

	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}

	@Override
	public void delete(Long id) {
		clientRepository.deleteById(id);
	}
}
